<?php

namespace Drupal\video_stream\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileVideoFormatter;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Template\Attribute;

/**
 * Plugin implementation of the 'video_streaming' formatter.
 *
 * @FieldFormatter(
 *   id = "video_streaming",
 *   label = @Translation("Video streaming"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class VideoStreaming extends FileVideoFormatter {

  /**
   * {@inheritdoc}
   */
  public static function getMediaType() {
    return 'video';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [
      // Implement settings summary.
    ] + parent::settingsSummary();
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareAttributes(array $additional_attributes = []) {
    return parent::prepareAttributes(['muted'])
      ->setAttribute('width', $this->getSetting('width'))
      ->setAttribute('height', $this->getSetting('height'));
  }

  /**
   * Gets source files with attributes.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items
   *   The item list.
   * @param string $langcode
   *   The language code of the referenced entities to display.
   *
   * @return array
   *   Numerically indexed array, which again contains an associative array with
   *   the following key/values:
   *     - file => \Drupal\file\Entity\File
   *     - source_attributes => \Drupal\Core\Template\Attribute
   */
  protected function getSourceFiles(EntityReferenceFieldItemListInterface $items, $langcode) {
    $source_files = [];
    // Because we can have the files grouped in a single media tag, we do a
    // grouping in case the multiple file behavior is not 'tags'.
    /** @var \Drupal\file\Entity\File $file */
    foreach ($this->getEntitiesToView($items, $langcode) as $file) {
      if (static::mimeTypeApplies($file->getMimeType())) {
        $source_attributes = new Attribute();
        $encryption_service = \Drupal::service('encryption');
        $encrypted_value = $encryption_service->encrypt($file->id());

        $source_attributes
          ->setAttribute('src', "/video_stream/" . urlencode($encrypted_value))
          ->setAttribute('type', $file->getMimeType());
        if ($this->getSetting('multiple_file_display_type') === 'tags') {
          $source_files[] = [
            [
              'file' => $file,
              'source_attributes' => $source_attributes,
            ],
          ];
        }
        else {
          $source_files[0][] = [
            'file' => $file,
            'source_attributes' => $source_attributes,
          ];
        }
      }
    }
    return $source_files;
  }

}
