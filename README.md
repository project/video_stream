INTRODUCTION
------------

This module provides a Video Stream Widget for file field types. 


REQUIREMENTS
------------

* Drupal file field
* Encryption module https://www.drupal.org/project/encryption


INSTALLATION
------------

* The video file id is encrypted by the encryption 
https://www.drupal.org/project/encryption module. Therefore you need to install
the module first and then generate an encryption key and finally put it on the
settings.php file. The details are explained on the encryption module project
page.
* Enable the Video Stream Module.


CONFIGURATION
-------------

* Add a file field to your entity and then go to "Manage Display" tab on your 
entity field settings. From the Format settings choose "Video streaming" and
change the configuration to fit your requirements.


MAINTAINERS
-----------

Current maintainers:
 * Bekir Dağ (bekirdag) - https://www.drupal.org/u/bekirdag
